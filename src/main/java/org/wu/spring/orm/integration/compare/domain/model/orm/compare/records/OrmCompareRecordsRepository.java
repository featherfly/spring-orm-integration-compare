package org.wu.spring.orm.integration.compare.domain.model.orm.compare.records;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;

import java.util.List;


/**
 * describe Orm 操作记录 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository 
 **/

public interface OrmCompareRecordsRepository {


    /**
     * describe 新增Orm 操作记录
     *
     * @param ormCompareRecords 新增Orm 操作记录     
     * @return {@link  Result<OrmCompareRecords>} Orm 操作记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
    Result<OrmCompareRecords> story(OrmCompareRecords ormCompareRecords);

    /**
     * describe 批量新增Orm 操作记录
     *
     * @param ormCompareRecordsList 批量新增Orm 操作记录     
     * @return {@link Result <List<OrmCompareRecords>>} Orm 操作记录新增后领域对象集合
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<List<OrmCompareRecords>> batchStory(List<OrmCompareRecords> ormCompareRecordsList);

    /**
     * describe 查询单个Orm 操作记录
     *
     * @param ormCompareRecords 查询单个Orm 操作记录     
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<OrmCompareRecords> findOne(OrmCompareRecords ormCompareRecords);

    /**
     * describe 查询多个Orm 操作记录
     *
     * @param ormCompareRecords 查询多个Orm 操作记录     
     * @return {@link Result<List<OrmCompareRecords>>} Orm 操作记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<List<OrmCompareRecords>> findList(OrmCompareRecords ormCompareRecords);

    /**
     * describe 分页查询多个Orm 操作记录
     *
     * @param size 当前页数
     * @param current 当前页
     * @param ormCompareRecords 分页查询多个Orm 操作记录     
     * @return {@link Result<LazyPage<OrmCompareRecords>>} 分页Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<LazyPage<OrmCompareRecords>> findPage(int size, int current, OrmCompareRecords ormCompareRecords);

    /**
     * describe 删除Orm 操作记录
     *
     * @param ormCompareRecords 删除Orm 操作记录     
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<OrmCompareRecords> remove(OrmCompareRecords ormCompareRecords);

    /**
     * describe 是否存在Orm 操作记录
     *
     * @param ormCompareRecords 是否存在Orm 操作记录     
     * @return {@link Result<Boolean>} Orm 操作记录是否存在     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    Result<Boolean> exists(OrmCompareRecords ormCompareRecords);

}