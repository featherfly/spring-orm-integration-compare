package org.wu.spring.orm.integration.compare.infrastructure.mapper.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import java.sql.Timestamp;

@Repository
public interface SysUserJpaRepository extends JpaRepository<SysUserDO, Long>, BatchSaveRepository<SysUserDO> {


}
