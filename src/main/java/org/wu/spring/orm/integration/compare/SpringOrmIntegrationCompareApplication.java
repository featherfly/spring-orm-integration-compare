package org.wu.spring.orm.integration.compare;

import com.dream.boot.bean.ConfigurationBean;
import com.dream.flex.annotation.EnableFlexAPT;
import org.beetl.ext.spring6.EnableBeetl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.wu.framework.lazy.orm.core.stereotype.LazyScan;
import org.wu.spring.orm.integration.compare.infrastructure.mapper.jpa.BatchSaveRepositoryImpl;

import java.util.Arrays;
import java.util.List;

@EnableJpaRepositories(
        repositoryBaseClass = BatchSaveRepositoryImpl.class
) // jpa
@LazyScan(scanBasePackages ="org.wu.spring.orm.integration.compare.infrastructure.entity" )
@SpringBootApplication()
@EnableBeetl
@EnableFlexAPT // dream_orm
public class SpringOrmIntegrationCompareApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringOrmIntegrationCompareApplication.class, args);
    }

    /**
     * 配置扫描的table和mapper路径 dream orm
     *
     * @return
     */
    @Bean
    public ConfigurationBean configurationBean() {
        String packageName = this.getClass().getPackage().getName();
        List<String> pathList = Arrays.asList(packageName);
        ConfigurationBean configurationBean = new ConfigurationBean(pathList, pathList);
        return configurationBean;
    }

}


