package org.wu.spring.orm.integration.compare.infrastructure.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.v3.oas.annotations.media.Schema;
import org.wu.framework.lazy.orm.core.stereotype.*;

import java.lang.Integer;
import java.time.LocalDateTime;
import java.lang.String;

/**
 * describe Orm 操作记录
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity
 **/
@Data
@Accessors(chain = true)
@LazyTable(tableName = "orm_compare_records",  comment = "Orm 操作记录")
@Schema(title = "orm_compare_records", description = "Orm 操作记录")
public class OrmCompareRecordsDO {


    /**
     * 影响行数
     */
    @Schema(description = "影响行数", name = "affectsRows", example = "")

    @LazyTableFieldUnique(name = "affects_rows", comment = "影响行数", columnType = "int")
    private Integer affectsRows;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    @LazyTableFieldCreateTime
    private LocalDateTime createTime;

    /**
     * 执行结果
     */
    @LazyTableField(name = "success", comment = "执行结果", columnType = "int")
    private Boolean success;

    /**
     * 主键
     */
    @Schema(description = "主键", name = "id", example = "")
    @LazyTableFieldId
    private Integer id;

    /**
     * 是否删除
     */
    @Schema(description = "是否删除", name = "isDeleted", example = "")
    @LazyTableField(name = "is_deleted", comment = "是否删除")
    private Boolean isDeleted;

    /**
     * 执行时间
     */
    @Schema(description = "执行时间", name = "operationTime", example = "")
    @LazyTableField(name = "operation_time", comment = "执行时间", columnType = "int")
    private Long operationTime;

    /**
     * orm(mybatis、lazy、sqltoy)
     */
    @Schema(description = "orm(mybatis、lazy、sqltoy)", name = "orm", example = "")
    @LazyTableFieldUnique(name = "orm", comment = "orm(mybatis、lazy、sqltoy)", columnType = "varchar(255)")
    private String orm;

    /**
     * 类型，增加、删除、修改、查询
     */
    @Schema(description = "类型，增加、删除、修改、查询", name = "type", example = "")
    @LazyTableFieldUnique(name = "type", comment = "类型，增加、删除、修改、查询", columnType = "varchar(255)")
    private String type;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间", name = "updateTime", example = "")
    @LazyTableFieldUpdateTime
    private LocalDateTime updateTime;

}