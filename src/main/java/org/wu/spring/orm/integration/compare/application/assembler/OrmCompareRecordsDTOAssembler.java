package org.wu.spring.orm.integration.compare.application.assembler;

import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecords;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsStoryCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsUpdateCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.orm.compare.records.OrmCompareRecordsQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.dto.OrmCompareRecordsDTO;
import org.mapstruct.factory.Mappers;
import org.mapstruct.Mapper;
/**
 * describe Orm 操作记录 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyAssembler 
 **/
@Mapper
public interface OrmCompareRecordsDTOAssembler {


    /**
     * describe MapStruct 创建的代理对象
     *
     
     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
    OrmCompareRecordsDTOAssembler INSTANCE = Mappers.getMapper(OrmCompareRecordsDTOAssembler.class);
    /**
     * describe 应用层存储入参转换成 领域对象
     *
     * @param ormCompareRecordsStoryCommand 保存Orm 操作记录对象     
     * @return {@link OrmCompareRecords} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecords toOrmCompareRecords(OrmCompareRecordsStoryCommand ormCompareRecordsStoryCommand);
    /**
     * describe 应用层更新入参转换成 领域对象
     *
     * @param ormCompareRecordsUpdateCommand 更新Orm 操作记录对象     
     * @return {@link OrmCompareRecords} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecords toOrmCompareRecords(OrmCompareRecordsUpdateCommand ormCompareRecordsUpdateCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param ormCompareRecordsQueryOneCommand 查询单个Orm 操作记录对象参数     
     * @return {@link OrmCompareRecords} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecords toOrmCompareRecords(OrmCompareRecordsQueryOneCommand ormCompareRecordsQueryOneCommand);
    /**
     * describe 应用层查询入参转换成 领域对象
     *
     * @param ormCompareRecordsQueryListCommand 查询集合Orm 操作记录对象参数     
     * @return {@link OrmCompareRecords} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecords toOrmCompareRecords(OrmCompareRecordsQueryListCommand ormCompareRecordsQueryListCommand);
    /**
     * describe 应用层删除入参转换成 领域对象
     *
     * @param ormCompareRecordsRemoveCommand 删除Orm 操作记录对象参数     
     * @return {@link OrmCompareRecords} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecords toOrmCompareRecords(OrmCompareRecordsRemoveCommand ormCompareRecordsRemoveCommand);
    /**
     * describe 持久层领域对象转换成DTO对象
     *
     * @param ormCompareRecords Orm 操作记录领域对象     
     * @return {@link OrmCompareRecordsDTO} Orm 操作记录DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
     OrmCompareRecordsDTO fromOrmCompareRecords(OrmCompareRecords ormCompareRecords);
}