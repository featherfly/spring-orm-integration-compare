package org.wu.spring.orm.integration.compare.application;


import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserRemoveCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserStoryCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserUpdateCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryListCommand;
import org.wu.spring.orm.integration.compare.application.command.sys.user.SysUserQueryOneCommand;
import org.wu.spring.orm.integration.compare.application.dto.SysUserDTO;
import java.util.List;

/**
 * describe sys_user 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyApplication 
 **/

public interface SysUserApplication {


    /**
     * describe 新增
     *
     * @param sysUserStoryCommand 新增     
     * @return {@link Result<SysUser>} 新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/
    Result<SysUser> story(SysUserStoryCommand sysUserStoryCommand);

    /**
     * describe 批量新增
     *
     * @param sysUserStoryCommandList 批量新增     
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<List<SysUser>> batchStory(List<SysUserStoryCommand> sysUserStoryCommandList);

    /**
     * describe 更新
     *
     * @param sysUserUpdateCommand 更新     
     * @return {@link Result<SysUser>} 领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<SysUser> updateOne(SysUserUpdateCommand sysUserUpdateCommand);

    /**
     * describe 查询单个
     *
     * @param sysUserQueryOneCommand 查询单个     
     * @return {@link Result<SysUserDTO>} DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<SysUserDTO> findOne(SysUserQueryOneCommand sysUserQueryOneCommand);

    /**
     * describe 查询多个
     *
     * @param sysUserQueryListCommand 查询多个     
     * @return {@link Result <List<SysUserDTO>>} DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result <List<SysUserDTO>> findList(SysUserQueryListCommand sysUserQueryListCommand);

    /**
     * describe 分页查询多个
     *
     * @param sysUserQueryListCommand 分页查询多个     
     * @return {@link Result <LazyPage<SysUserDTO>>} 分页DTO对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result <LazyPage<SysUserDTO>> findPage(int size, int current, SysUserQueryListCommand sysUserQueryListCommand);

    /**
     * describe 删除
     *
     * @param sysUserRemoveCommand 删除     
     * @return {@link Result<SysUser>}      
     
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    Result<SysUser> remove(SysUserRemoveCommand sysUserRemoveCommand);

}