package org.wu.spring.orm.integration.compare.infrastructure.mapper.beetlsql;

import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

import java.util.List;

@SqlResource("SysUserBeetlSqlMapper")
public interface SysUserBeetlSqlMapper extends BaseMapper<SysUserDO> {

    void story(@Param("item")SysUserDO sysUserDO);

    void batchStory(@Param("list")List<SysUserDO> sysUserDOList);
}
