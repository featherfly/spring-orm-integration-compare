package org.wu.spring.orm.integration.compare.infrastructure.persistence.dream;


import com.dream.system.config.Page;
import com.dream.template.mapper.TemplateMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;
import java.time.LocalDateTime;
import java.util.List;


/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("dreamOrmSqlComparisonRepository")
public class DreamOrmSqlComparisonRepository extends SysUserRepositoryAbstractRecord {


    @Resource
    private TemplateMapper templateMapper;



    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result <SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            // 遇到唯一性索引 用户名、scope
            templateMapper.insert(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DREAM_ORM, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(SysUserConverter.INSTANCE::fromSysUser).toList();
            templateMapper.batchInsert(sysUserDOList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DREAM_ORM, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserCondition sysUserCondition = new SysUserCondition();
            sysUserCondition.setId(sysUser.getId());
            SysUserDO sysUserDO = templateMapper
                    .selectOne(SysUserDO.class, sysUserCondition);

            SysUser sysUserOne = SysUserConverter.INSTANCE.toSysUser(sysUserDO);
            return ResultFactory.successOf(sysUserOne);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.DREAM_ORM, Type.findOne, startTime, endTime, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            SysUserCondition sysUserCondition = new SysUserCondition();

                        if (!ObjectUtils.isEmpty(sysUser.getId())) {
                            sysUserCondition.setId(sysUser.getId());
            }

            if (!ObjectUtils.isEmpty(sysUser.getIsDeleted())) {
                sysUserCondition.setIsDeleted(sysUser.getIsDeleted());
            }
            if (!ObjectUtils.isEmpty(sysUser.getUsername())) {
                sysUserCondition.setUsername(sysUser.getUsername());
            }
            List<SysUserDO> sysUserDOList = templateMapper.selectList(SysUserDO.class, sysUserCondition);


            List<SysUser> sysUserList = sysUserDOList.stream().map(SysUserConverter.INSTANCE::toSysUser).toList();
            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.DREAM_ORM, Type.findList, startTime, endTime, success);

        }
        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            // 构建分页对象，每页 3 条数据(默认第一页的页码为 0)
            Page page = new Page(current, size);
            SysUserCondition sysUserCondition = new SysUserCondition();
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserCondition.setId(sysUser.getId());
            }

            Page<SysUser> sysUserDOPageResult = templateMapper.selectPage(SysUser.class, sysUserCondition, page);

            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            lazyPage.setTotal(sysUserDOPageResult.getTotal());
            lazyPage.setRecord(sysUserDOPageResult.getRows().stream().toList());
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.DREAM_ORM, Type.findPage, startTime, endTime, size, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        SysUserCondition sysUserCondition = new SysUserCondition();
        sysUserCondition.setId(sysUser.getId());
        try {
            templateMapper.deleteById(SysUserDO.class, sysUserCondition);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DREAM_ORM, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();
        try {

            SysUserCondition sysUserCondition = new SysUserCondition();
            if (!ObjectUtils.isEmpty(sysUser.getId())) {
                sysUserCondition.setId(sysUser.getId());
            }
            exists = templateMapper.exist(SysUserDO.class, sysUserCondition);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.DREAM_ORM, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}