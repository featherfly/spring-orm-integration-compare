package org.wu.spring.orm.integration.compare.domain.model.sys.user;

import org.wu.framework.web.response.Result;

import java.util.List;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository
 **/

public interface SysUserRepository extends ORMRepository {

}