package org.wu.spring.orm.integration.compare.infrastructure.persistence.jpa;


import jakarta.annotation.Resource;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.domain.model.sys.user.SysUser;
import org.wu.spring.orm.integration.compare.infrastructure.converter.SysUserConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;
import org.wu.spring.orm.integration.compare.infrastructure.mapper.jpa.SysUserJpaRepository;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.SysUserRepositoryAbstractRecord;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Orm;
import org.wu.spring.orm.integration.compare.infrastructure.persistence.enums.Type;

import java.time.LocalDateTime;
import java.util.List;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence
 **/
@Component("jpa")
public class JpaComparisonRepository extends SysUserRepositoryAbstractRecord {

    @Resource
    SysUserJpaRepository sysUserJpaRepository;

    /**
     * describe 新增
     *
     * @param sysUser 新增
     * @return {@link Result<SysUser>} 新增后领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> story(SysUser sysUser) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            // 遇到唯一性索引 用户名、scope
            sysUserJpaRepository.batchIgnoreSave(List.of(sysUserDO),1);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JPA, Type.story, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增
     *
     * @param sysUserList 批量新增
     * @return {@link Result<List<SysUser>>} 新增后领域对象集合
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> batchStory(List<SysUser> sysUserList) throws Exception {
        boolean success = true;
        super.resetTestTableRecords();
        LocalDateTime startTime = LocalDateTime.now();
        try {
            List<SysUserDO> sysUserDOList = sysUserList.stream().map(SysUserConverter.INSTANCE::fromSysUser).toList();
            sysUserJpaRepository.batchIgnoreSave(sysUserDOList, 10000);

        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JPA, Type.batchStory, startTime, endTime, sysUserList.size(), success);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个
     *
     * @param sysUser 查询单个
     * @return {@link Result<SysUser>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> findOne(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findOne(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO fromSysUser = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            Example<SysUserDO> sysUserDOExample = Example.of(fromSysUser);
            SysUserDO sysUserDO = sysUserJpaRepository.findOne(sysUserDOExample).get();
            SysUser sysUserOne = SysUserConverter.INSTANCE.toSysUser(sysUserDO);
            return ResultFactory.successOf(sysUserOne);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.JPA, Type.findOne, startTime, endTime, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 查询多个
     *
     * @param sysUser 查询多个
     * @return {@link Result<List<SysUser>>} 领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<List<SysUser>> findList(SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            SysUserDO fromSysUser = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            ExampleMatcher matching = ExampleMatcher.matching();
            List<SysUserDO> sysUserDOList = sysUserJpaRepository
                    .findBy(Example.of(fromSysUser,matching),fetchableFluentQuery -> fetchableFluentQuery.stream().toList());

            List<SysUser> sysUserList = sysUserDOList.stream().map(SysUserConverter.INSTANCE::toSysUser).toList();
            return ResultFactory.successOf(sysUserList);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.JPA, Type.findList, startTime, endTime, success);

        }
        return ResultFactory.successOf();

    }

    /**
     * describe 分页查询多个
     *
     * @param size    当前页数
     * @param current 当前页
     * @param sysUser 分页查询多个
     * @return {@link Result<LazyPage<SysUser>>} 分页领域对象
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<LazyPage<SysUser>> findPage(int size, int current, SysUser sysUser) throws Exception {
        boolean success = true;
        super.findList(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {

            Pageable pageable = PageRequest.of(current-1, size);
            SysUserDO fromSysUser = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            ExampleMatcher matching = ExampleMatcher.matching();
            org.springframework.data.domain.Page<SysUserDO> sysUserDOPage = sysUserJpaRepository
                    .findAll(Example.of(fromSysUser, matching), pageable);


            List<SysUserDO> records = sysUserDOPage.getContent();
            long total = sysUserDOPage.getTotalPages();

            LazyPage<SysUser> lazyPage = new LazyPage<>(current, size);
            lazyPage.setTotal(total);
            lazyPage.setRecord(records.stream().map(SysUserConverter.INSTANCE::toSysUser).toList());
            return ResultFactory.successOf(lazyPage);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        } finally {
            LocalDateTime endTime = LocalDateTime.now();
            storyRecord(Orm.JPA, Type.findPage, startTime, endTime, size, success);
        }
        return ResultFactory.successOf();
    }

    /**
     * describe 删除
     *
     * @param sysUser 删除
     * @return {@link Result<SysUser>}
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<SysUser> remove(SysUser sysUser) throws Exception {
        boolean success = true;
        super.remove(sysUser);
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO sysUserDO = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            sysUserJpaRepository.delete(sysUserDO);
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }

        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JPA, Type.remove, startTime, endTime, success);
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在
     *
     * @param sysUser 领域对象
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在
     * @author Jia wei Wu
     * @date 2024/02/28 11:27 上午
     **/

    @Override
    public Result<Boolean> exists(SysUser sysUser) throws Exception {
        boolean success = true;
        super.exists(sysUser);
        boolean exists = false;
        LocalDateTime startTime = LocalDateTime.now();
        try {
            SysUserDO fromSysUser = SysUserConverter.INSTANCE.fromSysUser(sysUser);
            exists = sysUserJpaRepository.exists(Example.of(fromSysUser));
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }


        LocalDateTime endTime = LocalDateTime.now();
        storyRecord(Orm.JPA, Type.exists, startTime, endTime, success);
        return ResultFactory.successOf(exists);
    }

}