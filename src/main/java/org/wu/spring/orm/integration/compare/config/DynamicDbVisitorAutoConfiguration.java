

package org.wu.spring.orm.integration.compare.config;

import net.hasor.dbvisitor.dal.repository.DalRegistry;
import net.hasor.dbvisitor.dal.session.DalSession;
import net.hasor.dbvisitor.spring.boot.DbVisitorAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.wu.framework.lazy.orm.core.config.prop.LazyDataSourceAttribute;
import org.wu.framework.lazy.orm.database.sql.expand.database.persistence.factory.LazyDataSourcePropertiesFactory;

import javax.sql.DataSource;

/**
 * @see DbVisitorAutoConfiguration
 */
//@Configuration
public class DynamicDbVisitorAutoConfiguration  {

    @Bean
    public DalSession dalSession(DataSource dataSource, DalRegistry dalRegistry, LazyDataSourceAttribute lazyDataSourceAttribute) throws Exception {
        // 初始化数据库 防止数据库不存在
        LazyDataSourcePropertiesFactory.initSchema(lazyDataSourceAttribute);
        return  new DalSession(dataSource, dalRegistry);
    }


}
