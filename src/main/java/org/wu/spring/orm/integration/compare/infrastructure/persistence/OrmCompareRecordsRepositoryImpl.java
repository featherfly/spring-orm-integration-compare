package org.wu.spring.orm.integration.compare.infrastructure.persistence;

import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.response.ResultFactory;
import org.wu.spring.orm.integration.compare.infrastructure.converter.OrmCompareRecordsConverter;
import org.wu.spring.orm.integration.compare.infrastructure.entity.OrmCompareRecordsDO;

import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecordsRepository;
import org.springframework.stereotype.Repository;
import java.util.stream.Collectors;
import jakarta.annotation.Resource;
import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecords;
import java.util.List;

/**
 * describe Orm 操作记录 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructurePersistence 
 **/
@Repository
public class OrmCompareRecordsRepositoryImpl  implements  OrmCompareRecordsRepository {

    @Resource
    LazyLambdaStream lazyLambdaStream;

    /**
     * describe 新增Orm 操作记录
     *
     * @param ormCompareRecords 新增Orm 操作记录     
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录新增后领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/
    @Override
    public Result<OrmCompareRecords> story(OrmCompareRecords ormCompareRecords) {
        OrmCompareRecordsDO ormCompareRecordsDO = OrmCompareRecordsConverter.INSTANCE.fromOrmCompareRecords(ormCompareRecords);
        lazyLambdaStream.upsert(ormCompareRecordsDO);
        return ResultFactory.successOf();
    }

    /**
     * describe 批量新增Orm 操作记录
     *
     * @param ormCompareRecordsList 批量新增Orm 操作记录     
     * @return {@link Result<List<OrmCompareRecords>>} Orm 操作记录新增后领域对象集合     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<List<OrmCompareRecords>> batchStory(List<OrmCompareRecords> ormCompareRecordsList) {
        List<OrmCompareRecordsDO> ormCompareRecordsDOList = ormCompareRecordsList.stream().map(OrmCompareRecordsConverter.INSTANCE::fromOrmCompareRecords).collect(Collectors.toList());
        lazyLambdaStream.upsert(ormCompareRecordsDOList);
        return ResultFactory.successOf();
    }

    /**
     * describe 查询单个Orm 操作记录
     *
     * @param ormCompareRecords 查询单个Orm 操作记录     
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<OrmCompareRecords> findOne(OrmCompareRecords ormCompareRecords) {
        OrmCompareRecordsDO ormCompareRecordsDO = OrmCompareRecordsConverter.INSTANCE.fromOrmCompareRecords(ormCompareRecords);
        OrmCompareRecords ormCompareRecordsOne = lazyLambdaStream.selectOne(LazyWrappers.lambdaWrapperBean(ormCompareRecordsDO), OrmCompareRecords.class);
        return ResultFactory.successOf(ormCompareRecordsOne);
    }

    /**
     * describe 查询多个Orm 操作记录
     *
     * @param ormCompareRecords 查询多个Orm 操作记录     
     * @return {@link Result<List<OrmCompareRecords>>} Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<List<OrmCompareRecords>> findList(OrmCompareRecords ormCompareRecords) {
        OrmCompareRecordsDO ormCompareRecordsDO = OrmCompareRecordsConverter.INSTANCE.fromOrmCompareRecords(ormCompareRecords);
        List<OrmCompareRecords> ormCompareRecordsList = lazyLambdaStream.selectList(LazyWrappers.lambdaWrapperBean(ormCompareRecordsDO), OrmCompareRecords.class);
        return ResultFactory.successOf(ormCompareRecordsList);
    }

    /**
     * describe 分页查询多个Orm 操作记录
     *
     * @param size 当前页数
     * @param current 当前页
     * @param ormCompareRecords 分页查询多个Orm 操作记录     
     * @return {@link Result<LazyPage<OrmCompareRecords>>} 分页Orm 操作记录领域对象     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<LazyPage<OrmCompareRecords>> findPage(int size,int current,OrmCompareRecords ormCompareRecords) {
        OrmCompareRecordsDO ormCompareRecordsDO = OrmCompareRecordsConverter.INSTANCE.fromOrmCompareRecords(ormCompareRecords);
        LazyPage<OrmCompareRecords> lazyPage = new LazyPage<>(current,size);
        LazyPage<OrmCompareRecords> ormCompareRecordsLazyPage = lazyLambdaStream.selectPage(LazyWrappers.lambdaWrapperBean(ormCompareRecordsDO),lazyPage, OrmCompareRecords.class);
        return ResultFactory.successOf(ormCompareRecordsLazyPage);
    }

    /**
     * describe 删除Orm 操作记录
     *
     * @param ormCompareRecords 删除Orm 操作记录     
     * @return {@link Result<OrmCompareRecords>} Orm 操作记录     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<OrmCompareRecords> remove(OrmCompareRecords ormCompareRecords) {
        OrmCompareRecordsDO ormCompareRecordsDO = OrmCompareRecordsConverter.INSTANCE.fromOrmCompareRecords(ormCompareRecords);
        //  lazyLambdaStream.delete(LazyWrappers.lambdaWrapperBean(ormCompareRecordsDO));
        return ResultFactory.successOf();
    }

    /**
     * describe 是否存在Orm 操作记录
     *
     * @param ormCompareRecords Orm 操作记录领域对象     
     * @return {@link Result<Boolean>} 是否存在 true 存在，false 不存在     
     
     * @author Jia wei Wu
     * @date 2024/02/28 01:02 下午
     **/

    @Override
    public Result<Boolean> exists(OrmCompareRecords ormCompareRecords) {
        OrmCompareRecordsDO ormCompareRecordsDO = OrmCompareRecordsConverter.INSTANCE.fromOrmCompareRecords(ormCompareRecords);
        Boolean exists=lazyLambdaStream.exists(LazyWrappers.lambdaWrapperBean(ormCompareRecordsDO));
        return ResultFactory.successOf(exists);
    }

}