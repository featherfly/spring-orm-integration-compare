package org.wu.spring.orm.integration.compare.config;

import com.gitee.qdbp.jdbc.api.QdbcBoot;
import com.gitee.qdbp.jdbc.biz.QdbcBootImpl;
import com.gitee.qdbp.jdbc.plugins.DbPluginContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 配置 QdbcBoot
 */
@Configuration
public class QdbcAutoConfiguration {

    @Bean
    public QdbcBoot qdbcBoot(DataSource dataSource) {
        return QdbcBootImpl.buildWith(dataSource, DbPluginContainer.defaults());
    }
}
