package org.wu.spring.orm.integration.compare.domain.model.sys.user;

/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyDomainRepository
 **/

public interface ORMComparisonRepository extends ORMRepository{

}