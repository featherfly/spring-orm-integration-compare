package org.wu.spring.orm.integration.compare.config;

import org.noear.wood.DbContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class WoodAutoConfiguration {


    @Bean
    public DbContext getDb(DataSource ds) {
        DbContext db = new DbContext( ds); //使用DataSource配置的示例
        return db;
    }


}
