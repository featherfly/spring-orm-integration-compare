package org.wu.spring.orm.integration.compare.infrastructure.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.Getter;
import org.beetl.sql.annotation.entity.AutoID;
import org.sagacity.sqltoy.config.annotation.Column;
import org.sagacity.sqltoy.config.annotation.Entity;
import org.sagacity.sqltoy.config.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.wu.framework.lazy.orm.core.stereotype.LazyFieldStrategy;
import org.wu.framework.lazy.orm.core.stereotype.LazyTable;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableField;
import org.wu.framework.lazy.orm.core.stereotype.LazyTableFieldId;

import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * describe sys_user
 *
 * @author Jia wei Wu
 * @date 2024/02/28 11:27 上午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyInfrastructureEntity
 **/
@Data
//@Accessors(chain = true) // todo 与beetlsql 反射方法不兼容
@LazyTable(tableName = "sys_user", comment = "") // lazy
@TableName(value = "sys_user", autoResultMap = true)// mybatis  autoResultMap 处理json数据
@Entity(tableName = "sys_user") // sqltoy
@Schema(title = "sys_user", description = "")
@com.mybatisflex.annotation.Table(value = "sys_user") // flex
@com.easy.query.core.annotation.Table("sys_user") // easy-query
@cn.mybatis.mp.db.annotations.Table("sys_user") // mybatis-mp

// jpa
@jakarta.persistence.Entity// jpa
@jakarta.persistence.Table(name = "sys_user") //jpa
@net.hasor.dbvisitor.mapping.Table("sys_user") // dbvisitor
//@SQLInsert(sql = "INSERT IGNORE INTO  sys_user (column_name, create_time, id, is_deleted, password, scope, status, update_time,username) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", check = ResultCheckStyle.COUNT)


@org.beetl.sql.annotation.entity.Table(name = "sys_user")// beetlsql
@com.dream.system.annotation.Table("sys_user") // dream_orm

@javax.persistence.Table(name = "sys_user") // hammer-sqldb
public class SysUserDO implements Serializable
        , Persistable<Long> // 优化jpa存储问题
{


    /**
     * 额外字段
     */
    @Schema(description = "额外字段", name = "columnName", example = "")
    @LazyTableField(name = "column_name", comment = "额外字段", columnType = "varchar(255)")
    @Column(name = "column_name", comment = "额外字段")// sqltoy
    @jakarta.persistence.Column(name = "column_name", columnDefinition = "额外字段")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name") // db_visitor
    @com.dream.system.annotation.Column("column_name") // dream_orm
    @javax.persistence.Column(name = "column_name") //hammer_sql
    private String columnName;
    /**
     * 创建时间
     */
    @Schema(description = "创建时间", name = "createTime", example = "")
    @LazyTableField(name = "create_time", comment = "创建时间", defaultValue = "CURRENT_TIMESTAMP", upsertStrategy = LazyFieldStrategy.NEVER, columnType = "datetime", extra = " on update CURRENT_TIMESTAMP")
    @Column(name = "create_time", comment = "创建时间", defaultValue = "CURRENT_TIMESTAMP")// sqltoy
    @jakarta.persistence.Column(name = "create_time", columnDefinition = "创建时间")// sqltoy
    @org.beetl.sql.annotation.entity.Column(value = "create_time")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "create_time") // db_visitor
    @com.dream.system.annotation.Column("create_time") // dream_orm
    @javax.persistence.Column(name = "create_time")// //hammer_sql
    private LocalDateTime createTime;

    /**
     * 用户ID
     */
    @Getter
    @Schema(description = "用户ID", name = "id", example = "")
    @LazyTableFieldId(name = "id", comment = "用户ID")
    @Id// sqltoy
    @Column(name = "id")// sqltoy
    @cn.mybatis.mp.db.annotations.TableId // mybatis-mp
    @jakarta.persistence.Id // jpa
    @jakarta.persistence.Column(name = "id")// jpa
    @AutoID
    @org.beetl.sql.annotation.entity.Column(value = "id")// beetlsql
    @com.dream.system.annotation.Column("id") // dream_orm
    @com.dream.system.annotation.Id() // dream_orm
    @javax.persistence.Id()
    private Long id;

    /**
     * null
     */
    @Schema(description = "null", name = "isDeleted", example = "")
    @LazyTableField(name = "is_deleted", comment = "null", columnType = "tinyint(1)")
    @Column(name = "is_deleted", comment = "null")// sqltoy
    @jakarta.persistence.Column(name = "is_deleted", columnDefinition = "null")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "is_deleted")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "is_deleted") // db_visitor
    @com.dream.system.annotation.Column("is_deleted") // dream_orm
    private Boolean isDeleted;

    /**
     * 密码
     */
    @Schema(description = "密码", name = "password", example = "")
    @LazyTableField(name = "password", comment = "密码", columnType = "varchar(255)")
    @Column(name = "password", comment = "密码")// sqltoy
    @jakarta.persistence.Column(name = "password", columnDefinition = "密码")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "password")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "password") // db_visitor
    @com.dream.system.annotation.Column("password") // dream_orm
    private String password;

    /**
     * null
     */
    @Schema(description = "null", name = "scope", example = "")
//    @LazyTableFieldUnique(name = "scope", comment = "null", columnType = "varchar(255)")
    @LazyTableField(name = "scope", comment = "null", columnType = "varchar(255)")
    @Column(name = "scope", comment = "null")// sqltoy
    @jakarta.persistence.Column(name = "scope", columnDefinition = "null")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "scope")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "scope") // db_visitor
    @com.dream.system.annotation.Column("scope") // dream_orm
    private String scope;

    /**
     * 状态
     */
    @Schema(description = "状态", name = "status", example = "")
    @LazyTableField(name = "status", comment = "状态", columnType = "tinyint(1)")
    @Column(name = "status", comment = "状态")// sqltoy
    @jakarta.persistence.Column(name = "status", columnDefinition = "状态")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "status")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "status") // db_visitor
    @com.dream.system.annotation.Column("status") // dream_orm
    private Boolean status;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间", name = "updateTime", example = "")
    @LazyTableField(name = "update_time", comment = "更新时间", defaultValue = "CURRENT_TIMESTAMP", upsertStrategy = LazyFieldStrategy.NEVER, columnType = "datetime", extra = " on update CURRENT_TIMESTAMP")
    @Column(name = "update_time", comment = "更新时间", defaultValue = "CURRENT_TIMESTAMP")// sqltoy
    @jakarta.persistence.Column(name = "update_time", columnDefinition = "更新时间")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "update_time")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "update_time") // db_visitor
    @com.dream.system.annotation.Column("update_time") // dream_orm
    private LocalDateTime updateTime;

    /**
     * 用户名
     */
    @Schema(description = "用户名", name = "username", example = "")
//    @LazyTableFieldUnique(name = "username", comment = "用户名", columnType = "varchar(255)")
    @LazyTableField(name = "username", comment = "用户名", columnType = "varchar(255)")
    @Column(name = "username", comment = "用户名")// sqltoy
    @jakarta.persistence.Column(name = "username", columnDefinition = "用户名")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "username")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "username") // db_visitor
    @com.dream.system.annotation.Column("username") // dream_orm
    private String username;
    /**
     * 额外字段1
     */
    @Schema(description = "额外字段1", name = "columnName1", example = "")
    @LazyTableField(name = "column_name_1", comment = "额外字段1", columnType = "varchar(255)")
    @Column(name = "column_name_1", comment = "额外字段1")// sqltoy
    @jakarta.persistence.Column(name = "column_name_1", columnDefinition = "额外字段1")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_1")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_1") // db_visitor
    @com.dream.system.annotation.Column("column_name_1") // dream_orm
    @javax.persistence.Column(name = "column_name_1") //hammer_sql
    private String columnName_1;
    /**
     * 额外字段2
     */
    @Schema(description = "额外字段2", name = "columnName2", example = "")
    @LazyTableField(name = "column_name_2", comment = "额外字段2", columnType = "varchar(255)")
    @Column(name = "column_name_2", comment = "额外字段2")// sqltoy
    @jakarta.persistence.Column(name = "column_name_2", columnDefinition = "额外字段2")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_2")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_2") // db_visitor
    @com.dream.system.annotation.Column("column_name_2") // dream_orm
    @javax.persistence.Column(name = "column_name_2") //hammer_sql
    private String columnName_2;
    /**
     * 额外字段3
     */
    @Schema(description = "额外字段3", name = "columnName3", example = "")
    @LazyTableField(name = "column_name_3", comment = "额外字段3", columnType = "varchar(255)")
    @Column(name = "column_name_3", comment = "额外字段3")// sqltoy
    @jakarta.persistence.Column(name = "column_name_3", columnDefinition = "额外字段3")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_3")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_3") // db_visitor
    @com.dream.system.annotation.Column("column_name_3") // dream_orm
    @javax.persistence.Column(name = "column_name_3") //hammer_sql
    private String columnName_3;
    /**
     * 额外字段4
     */
    @Schema(description = "额外字段4", name = "columnName4", example = "")
    @LazyTableField(name = "column_name_4", comment = "额外字段4", columnType = "varchar(255)")
    @Column(name = "column_name_4", comment = "额外字段4")// sqltoy
    @jakarta.persistence.Column(name = "column_name_4", columnDefinition = "额外字段4")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_4")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_4") // db_visitor
    @com.dream.system.annotation.Column("column_name_4") // dream_orm
    @javax.persistence.Column(name = "column_name_4") //hammer_sql
    private String columnName_4;
    /**
     * 额外字段5
     */
    @Schema(description = "额外字段5", name = "columnName5", example = "")
    @LazyTableField(name = "column_name_5", comment = "额外字段5", columnType = "varchar(255)")
    @Column(name = "column_name_5", comment = "额外字段5")// sqltoy
    @jakarta.persistence.Column(name = "column_name_5", columnDefinition = "额外字段5")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_5")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_5") // db_visitor
    @com.dream.system.annotation.Column("column_name_5") // dream_orm
    @javax.persistence.Column(name = "column_name_5") //hammer_sql
    private String columnName_5;
    /**
     * 额外字段6
     */
    @Schema(description = "额外字段6", name = "columnName6", example = "")
    @LazyTableField(name = "column_name_6", comment = "额外字段6", columnType = "varchar(255)")
    @Column(name = "column_name_6", comment = "额外字段6")// sqltoy
    @jakarta.persistence.Column(name = "column_name_6", columnDefinition = "额外字段6")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_6")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_6") // db_visitor
    @com.dream.system.annotation.Column("column_name_6") // dream_orm
    @javax.persistence.Column(name = "column_name_6") //hammer_sql
    private String columnName_6;
    /**
     * 额外字段7
     */
    @Schema(description = "额外字段7", name = "columnName7", example = "")
    @LazyTableField(name = "column_name_7", comment = "额外字段7", columnType = "varchar(255)")
    @Column(name = "column_name_7", comment = "额外字段7")// sqltoy
    @jakarta.persistence.Column(name = "column_name_7", columnDefinition = "额外字段7")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_7")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_7") // db_visitor
    @com.dream.system.annotation.Column("column_name_7") // dream_orm
    @javax.persistence.Column(name = "column_name_7") //hammer_sql
    private String columnName_7;
    /**
     * 额外字段8
     */
    @Schema(description = "额外字段8", name = "columnName8", example = "")
    @LazyTableField(name = "column_name_8", comment = "额外字段8", columnType = "varchar(255)")
    @Column(name = "column_name_8", comment = "额外字段8")// sqltoy
    @jakarta.persistence.Column(name = "column_name_8", columnDefinition = "额外字段8")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_8")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_8") // db_visitor
    @com.dream.system.annotation.Column("column_name_8") // dream_orm
    @javax.persistence.Column(name = "column_name_8") //hammer_sql
    private String columnName_8;
    /**
     * 额外字段9
     */
    @Schema(description = "额外字段9", name = "columnName9", example = "")
    @LazyTableField(name = "column_name_9", comment = "额外字段9", columnType = "varchar(255)")
    @Column(name = "column_name_9", comment = "额外字段9")// sqltoy
    @jakarta.persistence.Column(name = "column_name_9", columnDefinition = "额外字段9")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_9")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_9") // db_visitor
    @com.dream.system.annotation.Column("column_name_9") // dream_orm
    @javax.persistence.Column(name = "column_name_9") //hammer_sql
    private String columnName_9;
    /**
     * 额外字段10
     */
    @Schema(description = "额外字段10", name = "columnName10", example = "")
    @LazyTableField(name = "column_name_10", comment = "额外字段10", columnType = "varchar(255)")
    @Column(name = "column_name_10", comment = "额外字段10")// sqltoy
    @jakarta.persistence.Column(name = "column_name_10", columnDefinition = "额外字段10")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_10")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_10") // db_visitor
    @com.dream.system.annotation.Column("column_name_10") // dream_orm
    @javax.persistence.Column(name = "column_name_10") //hammer_sql
    private String columnName_10;
    /**
     * 额外字段11
     */
    @Schema(description = "额外字段11", name = "columnName11", example = "")
    @LazyTableField(name = "column_name_11", comment = "额外字段11", columnType = "varchar(255)")
    @Column(name = "column_name_11", comment = "额外字段11")// sqltoy
    @jakarta.persistence.Column(name = "column_name_11", columnDefinition = "额外字段11")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_11")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_11") // db_visitor
    @com.dream.system.annotation.Column("column_name_11") // dream_orm
    @javax.persistence.Column(name = "column_name_11") //hammer_sql
    private String columnName_11;
    /**
     * 额外字段12
     */
    @Schema(description = "额外字段12", name = "columnName12", example = "")
    @LazyTableField(name = "column_name_12", comment = "额外字段12", columnType = "varchar(255)")
    @Column(name = "column_name_12", comment = "额外字段12")// sqltoy
    @jakarta.persistence.Column(name = "column_name_12", columnDefinition = "额外字段12")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_12")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_12") // db_visitor
    @com.dream.system.annotation.Column("column_name_12") // dream_orm
    @javax.persistence.Column(name = "column_name_12") //hammer_sql
    private String columnName_12;
    /**
     * 额外字段13
     */
    @Schema(description = "额外字段13", name = "columnName13", example = "")
    @LazyTableField(name = "column_name_13", comment = "额外字段13", columnType = "varchar(255)")
    @Column(name = "column_name_13", comment = "额外字段13")// sqltoy
    @jakarta.persistence.Column(name = "column_name_13", columnDefinition = "额外字段13")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_13")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_13") // db_visitor
    @com.dream.system.annotation.Column("column_name_13") // dream_orm
    @javax.persistence.Column(name = "column_name_13") //hammer_sql
    private String columnName_13;
    /**
     * 额外字段14
     */
    @Schema(description = "额外字段14", name = "columnName14", example = "")
    @LazyTableField(name = "column_name_14", comment = "额外字段14", columnType = "varchar(255)")
    @Column(name = "column_name_14", comment = "额外字段14")// sqltoy
    @jakarta.persistence.Column(name = "column_name_14", columnDefinition = "额外字段14")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_14")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_14") // db_visitor
    @com.dream.system.annotation.Column("column_name_14") // dream_orm
    @javax.persistence.Column(name = "column_name_14") //hammer_sql
    private String columnName_14;
    /**
     * 额外字段15
     */
    @Schema(description = "额外字段15", name = "columnName15", example = "")
    @LazyTableField(name = "column_name_15", comment = "额外字段15", columnType = "varchar(255)")
    @Column(name = "column_name_15", comment = "额外字段15")// sqltoy
    @jakarta.persistence.Column(name = "column_name_15", columnDefinition = "额外字段15")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_15")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_15") // db_visitor
    @com.dream.system.annotation.Column("column_name_15") // dream_orm
    @javax.persistence.Column(name = "column_name_15") //hammer_sql
    private String columnName_15;
    /**
     * 额外字段16
     */
    @Schema(description = "额外字段16", name = "columnName16", example = "")
    @LazyTableField(name = "column_name_16", comment = "额外字段16", columnType = "varchar(255)")
    @Column(name = "column_name_16", comment = "额外字段16")// sqltoy
    @jakarta.persistence.Column(name = "column_name_16", columnDefinition = "额外字段16")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_16")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_16") // db_visitor
    @com.dream.system.annotation.Column("column_name_16") // dream_orm
    @javax.persistence.Column(name = "column_name_16") //hammer_sql
    private String columnName_16;
    /**
     * 额外字段17
     */
    @Schema(description = "额外字段17", name = "columnName17", example = "")
    @LazyTableField(name = "column_name_17", comment = "额外字段17", columnType = "varchar(255)")
    @Column(name = "column_name_17", comment = "额外字段17")// sqltoy
    @jakarta.persistence.Column(name = "column_name_17", columnDefinition = "额外字段17")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_17")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_17") // db_visitor
    @com.dream.system.annotation.Column("column_name_17") // dream_orm
    @javax.persistence.Column(name = "column_name_17") //hammer_sql
    private String columnName_17;
    /**
     * 额外字段18
     */
    @Schema(description = "额外字段18", name = "columnName18", example = "")
    @LazyTableField(name = "column_name_18", comment = "额外字段18", columnType = "varchar(255)")
    @Column(name = "column_name_18", comment = "额外字段18")// sqltoy
    @jakarta.persistence.Column(name = "column_name_18", columnDefinition = "额外字段18")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_18")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_18") // db_visitor
    @com.dream.system.annotation.Column("column_name_18") // dream_orm
    @javax.persistence.Column(name = "column_name_18") //hammer_sql
    private String columnName_18;
    /**
     * 额外字段19
     */
    @Schema(description = "额外字段19", name = "columnName19", example = "")
    @LazyTableField(name = "column_name_19", comment = "额外字段19", columnType = "varchar(255)")
    @Column(name = "column_name_19", comment = "额外字段19")// sqltoy
    @jakarta.persistence.Column(name = "column_name_19", columnDefinition = "额外字段19")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_19")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_19") // db_visitor
    @com.dream.system.annotation.Column("column_name_19") // dream_orm
    @javax.persistence.Column(name = "column_name_19") //hammer_sql
    private String columnName_19;
    /**
     * 额外字段20
     */
    @Schema(description = "额外字段20", name = "columnName20", example = "")
    @LazyTableField(name = "column_name_20", comment = "额外字段20", columnType = "varchar(255)")
    @Column(name = "column_name_20", comment = "额外字段20")// sqltoy
    @jakarta.persistence.Column(name = "column_name_20", columnDefinition = "额外字段20")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_20")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_20") // db_visitor
    @com.dream.system.annotation.Column("column_name_20") // dream_orm
    @javax.persistence.Column(name = "column_name_20") //hammer_sql
    private String columnName_20;
    /**
     * 额外字段21
     */
    @Schema(description = "额外字段21", name = "columnName21", example = "")
    @LazyTableField(name = "column_name_21", comment = "额外字段21", columnType = "varchar(255)")
    @Column(name = "column_name_21", comment = "额外字段21")// sqltoy
    @jakarta.persistence.Column(name = "column_name_21", columnDefinition = "额外字段21")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_21")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_21") // db_visitor
    @com.dream.system.annotation.Column("column_name_21") // dream_orm
    @javax.persistence.Column(name = "column_name_21") //hammer_sql
    private String columnName_21;
    /**
     * 额外字段22
     */
    @Schema(description = "额外字段22", name = "columnName22", example = "")
    @LazyTableField(name = "column_name_22", comment = "额外字段22", columnType = "varchar(255)")
    @Column(name = "column_name_22", comment = "额外字段22")// sqltoy
    @jakarta.persistence.Column(name = "column_name_22", columnDefinition = "额外字段22")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_22")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_22") // db_visitor
    @com.dream.system.annotation.Column("column_name_22") // dream_orm
    @javax.persistence.Column(name = "column_name_22") //hammer_sql
    private String columnName_22;
    /**
     * 额外字段23
     */
    @Schema(description = "额外字段23", name = "columnName23", example = "")
    @LazyTableField(name = "column_name_23", comment = "额外字段23", columnType = "varchar(255)")
    @Column(name = "column_name_23", comment = "额外字段23")// sqltoy
    @jakarta.persistence.Column(name = "column_name_23", columnDefinition = "额外字段23")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_23")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_23") // db_visitor
    @com.dream.system.annotation.Column("column_name_23") // dream_orm
    @javax.persistence.Column(name = "column_name_23") //hammer_sql
    private String columnName_23;
    /**
     * 额外字段24
     */
    @Schema(description = "额外字段24", name = "columnName24", example = "")
    @LazyTableField(name = "column_name_24", comment = "额外字段24", columnType = "varchar(255)")
    @Column(name = "column_name_24", comment = "额外字段24")// sqltoy
    @jakarta.persistence.Column(name = "column_name_24", columnDefinition = "额外字段24")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_24")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_24") // db_visitor
    @com.dream.system.annotation.Column("column_name_24") // dream_orm
    @javax.persistence.Column(name = "column_name_24") //hammer_sql
    private String columnName_24;
    /**
     * 额外字段25
     */
    @Schema(description = "额外字段25", name = "columnName25", example = "")
    @LazyTableField(name = "column_name_25", comment = "额外字段25", columnType = "varchar(255)")
    @Column(name = "column_name_25", comment = "额外字段25")// sqltoy
    @jakarta.persistence.Column(name = "column_name_25", columnDefinition = "额外字段25")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_25")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_25") // db_visitor
    @com.dream.system.annotation.Column("column_name_25") // dream_orm
    @javax.persistence.Column(name = "column_name_25") //hammer_sql
    private String columnName_25;
    /**
     * 额外字段26
     */
    @Schema(description = "额外字段26", name = "columnName26", example = "")
    @LazyTableField(name = "column_name_26", comment = "额外字段26", columnType = "varchar(255)")
    @Column(name = "column_name_26", comment = "额外字段26")// sqltoy
    @jakarta.persistence.Column(name = "column_name_26", columnDefinition = "额外字段26")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_26")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_26") // db_visitor
    @com.dream.system.annotation.Column("column_name_26") // dream_orm
    @javax.persistence.Column(name = "column_name_26") //hammer_sql
    private String columnName_26;
    /**
     * 额外字段27
     */
    @Schema(description = "额外字段27", name = "columnName27", example = "")
    @LazyTableField(name = "column_name_27", comment = "额外字段27", columnType = "varchar(255)")
    @Column(name = "column_name_27", comment = "额外字段27")// sqltoy
    @jakarta.persistence.Column(name = "column_name_27", columnDefinition = "额外字段27")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_27")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_27") // db_visitor
    @com.dream.system.annotation.Column("column_name_27") // dream_orm
    @javax.persistence.Column(name = "column_name_27") //hammer_sql
    private String columnName_27;
    /**
     * 额外字段28
     */
    @Schema(description = "额外字段28", name = "columnName28", example = "")
    @LazyTableField(name = "column_name_28", comment = "额外字段28", columnType = "varchar(255)")
    @Column(name = "column_name_28", comment = "额外字段28")// sqltoy
    @jakarta.persistence.Column(name = "column_name_28", columnDefinition = "额外字段28")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_28")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_28") // db_visitor
    @com.dream.system.annotation.Column("column_name_28") // dream_orm
    @javax.persistence.Column(name = "column_name_28") //hammer_sql
    private String columnName_28;
    /**
     * 额外字段29
     */
    @Schema(description = "额外字段29", name = "columnName29", example = "")
    @LazyTableField(name = "column_name_29", comment = "额外字段29", columnType = "varchar(255)")
    @Column(name = "column_name_29", comment = "额外字段29")// sqltoy
    @jakarta.persistence.Column(name = "column_name_29", columnDefinition = "额外字段29")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_29")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_29") // db_visitor
    @com.dream.system.annotation.Column("column_name_29") // dream_orm
    @javax.persistence.Column(name = "column_name_29") //hammer_sql
    private String columnName_29;
    /**
     * 额外字段30
     */
    @Schema(description = "额外字段30", name = "columnName30", example = "")
    @LazyTableField(name = "column_name_30", comment = "额外字段30", columnType = "varchar(255)")
    @Column(name = "column_name_30", comment = "额外字段30")// sqltoy
    @jakarta.persistence.Column(name = "column_name_30", columnDefinition = "额外字段30")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_30")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_30") // db_visitor
    @com.dream.system.annotation.Column("column_name_30") // dream_orm
    @javax.persistence.Column(name = "column_name_30") //hammer_sql
    private String columnName_30;
    /**
     * 额外字段31
     */
    @Schema(description = "额外字段31", name = "columnName31", example = "")
    @LazyTableField(name = "column_name_31", comment = "额外字段31", columnType = "varchar(255)")
    @Column(name = "column_name_31", comment = "额外字段31")// sqltoy
    @jakarta.persistence.Column(name = "column_name_31", columnDefinition = "额外字段31")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_31")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_31") // db_visitor
    @com.dream.system.annotation.Column("column_name_31") // dream_orm
    @javax.persistence.Column(name = "column_name_31") //hammer_sql
    private String columnName_31;
    /**
     * 额外字段32
     */
    @Schema(description = "额外字段32", name = "columnName32", example = "")
    @LazyTableField(name = "column_name_32", comment = "额外字段32", columnType = "varchar(255)")
    @Column(name = "column_name_32", comment = "额外字段32")// sqltoy
    @jakarta.persistence.Column(name = "column_name_32", columnDefinition = "额外字段32")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_32")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_32") // db_visitor
    @com.dream.system.annotation.Column("column_name_32") // dream_orm
    @javax.persistence.Column(name = "column_name_32") //hammer_sql
    private String columnName_32;
    /**
     * 额外字段33
     */
    @Schema(description = "额外字段33", name = "columnName33", example = "")
    @LazyTableField(name = "column_name_33", comment = "额外字段33", columnType = "varchar(255)")
    @Column(name = "column_name_33", comment = "额外字段33")// sqltoy
    @jakarta.persistence.Column(name = "column_name_33", columnDefinition = "额外字段33")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_33")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_33") // db_visitor
    @com.dream.system.annotation.Column("column_name_33") // dream_orm
    @javax.persistence.Column(name = "column_name_33") //hammer_sql
    private String columnName_33;
    /**
     * 额外字段34
     */
    @Schema(description = "额外字段34", name = "columnName34", example = "")
    @LazyTableField(name = "column_name_34", comment = "额外字段34", columnType = "varchar(255)")
    @Column(name = "column_name_34", comment = "额外字段34")// sqltoy
    @jakarta.persistence.Column(name = "column_name_34", columnDefinition = "额外字段34")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_34")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_34") // db_visitor
    @com.dream.system.annotation.Column("column_name_34") // dream_orm
    @javax.persistence.Column(name = "column_name_34") //hammer_sql
    private String columnName_34;
    /**
     * 额外字段35
     */
    @Schema(description = "额外字段35", name = "columnName35", example = "")
    @LazyTableField(name = "column_name_35", comment = "额外字段35", columnType = "varchar(255)")
    @Column(name = "column_name_35", comment = "额外字段35")// sqltoy
    @jakarta.persistence.Column(name = "column_name_35", columnDefinition = "额外字段35")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_35")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_35") // db_visitor
    @com.dream.system.annotation.Column("column_name_35") // dream_orm
    @javax.persistence.Column(name = "column_name_35") //hammer_sql
    private String columnName_35;
    /**
     * 额外字段36
     */
    @Schema(description = "额外字段36", name = "columnName36", example = "")
    @LazyTableField(name = "column_name_36", comment = "额外字段36", columnType = "varchar(255)")
    @Column(name = "column_name_36", comment = "额外字段36")// sqltoy
    @jakarta.persistence.Column(name = "column_name_36", columnDefinition = "额外字段36")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_36")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_36") // db_visitor
    @com.dream.system.annotation.Column("column_name_36") // dream_orm
    @javax.persistence.Column(name = "column_name_36") //hammer_sql
    private String columnName_36;
    /**
     * 额外字段37
     */
    @Schema(description = "额外字段37", name = "columnName37", example = "")
    @LazyTableField(name = "column_name_37", comment = "额外字段37", columnType = "varchar(255)")
    @Column(name = "column_name_37", comment = "额外字段37")// sqltoy
    @jakarta.persistence.Column(name = "column_name_37", columnDefinition = "额外字段37")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_37")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_37") // db_visitor
    @com.dream.system.annotation.Column("column_name_37") // dream_orm
    @javax.persistence.Column(name = "column_name_37") //hammer_sql
    private String columnName_37;
    /**
     * 额外字段38
     */
    @Schema(description = "额外字段38", name = "columnName38", example = "")
    @LazyTableField(name = "column_name_38", comment = "额外字段38", columnType = "varchar(255)")
    @Column(name = "column_name_38", comment = "额外字段38")// sqltoy
    @jakarta.persistence.Column(name = "column_name_38", columnDefinition = "额外字段38")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_38")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_38") // db_visitor
    @com.dream.system.annotation.Column("column_name_38") // dream_orm
    @javax.persistence.Column(name = "column_name_38") //hammer_sql
    private String columnName_38;
    /**
     * 额外字段39
     */
    @Schema(description = "额外字段39", name = "columnName39", example = "")
    @LazyTableField(name = "column_name_39", comment = "额外字段39", columnType = "varchar(255)")
    @Column(name = "column_name_39", comment = "额外字段39")// sqltoy
    @jakarta.persistence.Column(name = "column_name_39", columnDefinition = "额外字段39")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_39")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_39") // db_visitor
    @com.dream.system.annotation.Column("column_name_39") // dream_orm
    @javax.persistence.Column(name = "column_name_39") //hammer_sql
    private String columnName_39;
    /**
     * 额外字段40
     */
    @Schema(description = "额外字段40", name = "columnName40", example = "")
    @LazyTableField(name = "column_name_40", comment = "额外字段40", columnType = "varchar(255)")
    @Column(name = "column_name_40", comment = "额外字段40")// sqltoy
    @jakarta.persistence.Column(name = "column_name_40", columnDefinition = "额外字段40")// jpa
    @org.beetl.sql.annotation.entity.Column(value = "column_name_40")// beetlsql
    @net.hasor.dbvisitor.mapping.Column(name = "column_name_40") // db_visitor
    @com.dream.system.annotation.Column("column_name_40") // dream_orm
    @javax.persistence.Column(name = "column_name_40") //hammer_sql
    private String columnName_40;



    @net.hasor.dbvisitor.mapping.Ignore
    @Override
    public boolean isNew() {
        return true;
    }

}