package org.wu.spring.orm.integration.compare.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.wu.framework.web.response.Result;
import org.wu.framework.web.spring.EasyController;
import org.wu.spring.orm.integration.compare.application.OrmCompareRecordsApplication;
import org.wu.spring.orm.integration.compare.domain.model.orm.compare.records.OrmCompareRecordsEcharts;

/**
 * describe Orm 操作记录 
 *
 * @author Jia wei Wu
 * @date 2024/02/28 01:02 下午
 * @see org.wu.framework.lazy.orm.core.persistence.reverse.lazy.ddd.DefaultDDDLazyController 
 **/
@Tag(name = "Orm 操作记录提供者")
@EasyController("/orm/compare/records")
public class OrmCompareRecordsProvider  {

    @Resource
    private OrmCompareRecordsApplication ormCompareRecordsApplication;



    /**
     * describe 获取echarts数据
     *

     * @author Jia wei Wu
     **/

    @Operation(summary = "获取echarts数据")
    @GetMapping("/findEchartsData")
    public Result<OrmCompareRecordsEcharts> findEchartsData(@RequestParam("type")String type){
        return ormCompareRecordsApplication.findEchartsData(type);
    }

}