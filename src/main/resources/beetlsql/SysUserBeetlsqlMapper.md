story
===

```sql
        insert ignore into
        `sys_user` (`column_name`, `create_time`, `id`, `is_deleted`, `password`, `scope`, `status`, `update_time`,`username`)
        VALUES
            (#{item.columnName},#{item.createTime},#{item.id},#{item.isDeleted},#{item.password},#{item.scope},#{item.status},#{item.updateTime},#{item.username})

```

batchStory
===

```sql
      insert ignore into
        `sys_user` (`column_name`, `create_time`, `id`, `is_deleted`, `password`, `scope`, `status`, `update_time`,`username`)
        VALUES
        -- @for(item in list){
         (#{item.columnName},#{item.createTime},#{item.id},#{item.isDeleted},#{item.password},#{item.scope},#{item.status},#{item.updateTime},#{item.username})
         -- @}


```