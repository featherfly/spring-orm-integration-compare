package org.wu.spring.orm.integration.compare;


import org.springframework.util.StopWatch;
import org.wu.framework.core.utils.DataTransformUntil;
import org.wu.framework.lazy.orm.database.lambda.domain.LazyPage;
import org.wu.framework.lazy.orm.database.lambda.stream.lambda.LazyLambdaStream;
import org.wu.framework.lazy.orm.database.lambda.stream.wrapper.LazyWrappers;
import org.wu.framework.lazy.orm.core.config.enums.LazyDataSourceType;
import org.wu.framework.lazy.orm.core.toolkit.DynamicLazyDataSourceTypeHolder;
import org.wu.framework.lazy.orm.database.sql.expand.database.persistence.factory.LazyLambdaStreamFactory;
import org.wu.spring.orm.integration.compare.infrastructure.entity.SysUserDO;

public class LazyTest {
    public static void main(String[] args) {
        LazyLambdaStream lazyLambdaStream = LazyLambdaStreamFactory.createLazyLambdaStream("127.0.0.1", 3306, "compare_orm", "root", "wujiawei");

        DynamicLazyDataSourceTypeHolder.push(LazyDataSourceType.MySQL);
        // 清空数据
        lazyLambdaStream.delete(LazyWrappers.<SysUserDO>lambdaWrapper().notNull(SysUserDO::getId));
        StopWatch stopWatch = new StopWatch("Lazy ORM 执行数据操作");
        // 10 的幂数
        int power = 5;
        // 执行批量插入
        int size = 10;
        for (int i = 0; i < power; i++) {
            stopWatch.start("批量新增数据:" + size);
            lazyLambdaStream.upsert(DataTransformUntil.simulationBeanList(SysUserDO.class, size));
            stopWatch.stop();
            size = size * 10;
        }
        int pageSize = 10;
        // 执行 查询
        for (int i = 0; i < power; i++) {
            stopWatch.start("批量分页查询数据:" + pageSize);
            lazyLambdaStream.selectPage(LazyWrappers.<SysUserDO>lambdaWrapperBean(new SysUserDO()), LazyPage.of(1, pageSize));
            pageSize = pageSize * 10;
            stopWatch.stop();
        }
        String s = stopWatch.prettyPrint();
        System.out.println(s);

    }
}
